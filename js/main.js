var canvas = document.getElementById("canvas")
canvas.width = screen.width;
canvas.height = screen.height;
var width = canvas.width
var height = canvas.height
var ctx = canvas.getContext("2d")
ctx.fillStyle = "red"
var lastRender = 0;

var bonkContainer = [];
 
class Bonk{
  constructor(x,y,height,width,speed){
    this.height=height;
    this.width=width;
    this.x=x;
    this.y=y;
    this.speed = speed;
    this.directionx='right';
    this.directiony='up';
    this.halfWidth=this.width/2;
    this.halfHeight=this.height/2;
    this.inCollision=0;
  }

  draw(){
    ctx.fillRect(this.x, this.y, this.width, this.height)
  }
  

  lastCollision(){
    return this.inCollision;
  } 

  switchDirection(axis){
    if (axis == 'x'){
      if (this.directionx == 'right'){ 
        this.directionx = 'left';
      } else {
        this.directionx = 'right';
      }
    } else if (axis == 'y'){
      if (this.directiony == 'up'){
        this.directiony = 'down';
      } else {
        this.directiony = 'up';
      }
    }
  }

  update(progress){
    var progress = progress*this.speed

    if (this.directionx=='right'){
      this.x += progress
    } else{
      this.x -= progress
    }
    
    if (this.directiony=='up'){
      this.y -= progress
    } else{
      this.y += progress
    }
    // collision
    var collision = false; 
    for (var i =0; i< bonkContainer.length; i++){
      if ( bonkContainer[i]!== this){
        //xcollision
        if ((this.x + this.width) > bonkContainer[i].x &&
              this.x < (bonkContainer[i].x+bonkContainer[i].width)){
          //ycollision
          if ((this.y+this.height) > bonkContainer[i].y &&
                this.y < (bonkContainer[i].y+bonkContainer[i].height)){
            //collide 
            let collision = Date.now();
            if (collision - this.inCollision > 500){
              this.inCollision = collision; 
              this.switchDirection('x');
              this.switchDirection('y');
              bonkContainer[i].switchDirection('x');
             // bonkContainer[i].switchDirection('y');
            } 
          }
        }
      }
    }


    if (this.x +this.width > width) {
      //state.x -= width
      this.directionx='left';
    }
    else if (this.x < 0) {
      //state.x += width
      this.directionx='right';
    }

    if (this.y +this.height > height) {
     // state.y -= height
      this.directiony='up';
    }
    else if (this.y < 0) {
      //state.y += height
      this.directiony='down';
    }
  }
}

function update(progress){
  for (var i of bonkContainer){
    i.update(progress)
  } 
}

function draw(){
  ctx.clearRect(0, 0, width, height)
  for (var i of bonkContainer){
    i.draw()
  } 
}

function loop(timestamp) {
  var progress = timestamp - lastRender
  update(progress)
  draw()
  lastRender = timestamp
  window.requestAnimationFrame(loop)
}
bonkContainer.push(new Bonk(200,200,10,500,0.5));
bonkContainer.push(new Bonk(1,1000,300,10,0.1));
bonkContainer.push(new Bonk(1,1,50,50,0.01));
bonkContainer.push(new Bonk(100,100,100,100,1));
window.requestAnimationFrame(loop)

